---
title: Titel
subtitle: Untertitel
author: Aaron Wiedemer
date: 2020
---
---
lang: de-DE
toc: true
toc-depth: 2
lof: false
lot: false
suppress-bibliography: false
link-citations: true
papersize: A4
geometry: margin=3cm
linestretch: 1
mainfont: "Constantia"
sansfont: "Calibri"
fontsize: 11pt
---
---
bibliography: ref.bib
csl: universitat-freiburg-geschichte.csl
filters: pandoc-citeproc
pdf-engine: xelatex
header-includes: |
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyhead[R]{\slshape \leftmark}
    \fancyhead[L]{\slshape \rightmark}
    \renewcommand{\familydefault}{\rmdefault}
    \usepackage{blindtext}
    \usepackage{sectsty}
    \allsectionsfont{\sffamily}
---

# Überschrift 1
![Neovim ist der beste Texteditor](pic.png)

\Blinddocument

# Literaturverzeichnis
:::{#refs}
:::

\addcontentsline{toc}{section}{Abbildungsverzeichnis}
\listoffigures
\addcontentsline{toc}{section}{Dingens}
\listoftables
